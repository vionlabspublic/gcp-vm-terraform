/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# GSA + IAM part for Processing VM & GCP Cloud Run backend
#

locals {

  # GCP service account ids must be < 30 chars matching regex ^[a-z](?:[-a-z0-9]{4,28}[a-z0-9])$

  vm_gsa        = "vionlabs-vm-gsa"
  vm_gsa_fqn    = "serviceAccount:${google_service_account.vm_gsa.email}"

  cr_gsa_fqn    = "serviceAccount:${var.backend_service_account}"

  # Cloud Pub/Sub Service Account in current project
  # pubsub_svc_account_email = "service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"

}

data "google_project" "project" {
  project_id = var.project_id
}



#
# Vionlabs VM GSA + infra IAMs
#

resource "google_service_account" "vm_gsa" {

  account_id   = local.vm_gsa
  display_name = "GCP SA bound to Vionlabs Edge Processing VM"
  project      = var.project_id
}

# Grant access to output bucket operations
resource "google_storage_bucket_iam_member" "vm_gsa-output-bucket-iam" {

  bucket       = google_storage_bucket.output-bucket.name
  role         = "roles/storage.legacyBucketWriter"
  member       = local.vm_gsa_fqn
}
# some FEs need it
resource "google_storage_bucket_iam_member" "vm_gsa-output-bucket-iam-2" {

  bucket       = google_storage_bucket.output-bucket.name
  role         = "roles/storage.legacyObjectReader"
  member       = local.vm_gsa_fqn
}

# Grant access to secure bucket operations
resource "google_storage_bucket_iam_member" "vm_gsa-secure-bucket-iam" {

  bucket       = google_storage_bucket.secure-bucket.name
  role         = "roles/storage.legacyBucketWriter"
  member       = local.vm_gsa_fqn
}

# Grant access to publishing messages into events topic
resource "google_pubsub_topic_iam_member" "vm_gsa-events-iam" {

  project      = var.project_id
  topic        = google_pubsub_topic.topic-events.id
  role         = "roles/pubsub.publisher"
  member       = local.vm_gsa_fqn
}

# Grant access to reading messages from batches subscription
resource "google_pubsub_subscription_iam_member" "vm_gsa-batches-iam" {

  project      = var.project_id
  subscription = google_pubsub_subscription.batches-subs.id
  role         = "roles/pubsub.subscriber"
  member       = local.vm_gsa_fqn
}

# Grant service usage consumer role for VM GSA
resource "google_project_iam_member" "vm_gsa-serviceUsage-iam" {
  project      = var.project_id
  role         = "roles/serviceusage.serviceUsageConsumer"
  member       = local.vm_gsa_fqn
}


##########################################################################

#
# Vionlabs CloudRun backend GSA + IAMs
#
/* provisioned in Cloud Run TF!
resource "google_service_account" "cr_gsa" {

  account_id   = local.cr_gsa
  display_name = "GCP SA bound to Customer's Backend Cloud Run"
  project      = var.project_id
}
*/
# Grant access to output bucket operations - Read Only
resource "google_storage_bucket_iam_member" "cr_gsa-output-bucket-obj-reader-iam" {

  bucket       = google_storage_bucket.output-bucket.name
  role         = "roles/storage.legacyObjectReader"
  member       = local.cr_gsa_fqn
}
# CR GSA writes to output bucket some artifacts for some FE
resource "google_storage_bucket_iam_member" "cr_gsa-output-bucket-obj-writer-iam" {

  bucket       = google_storage_bucket.output-bucket.name
  role         = "roles/storage.legacyBucketWriter"
  member       = local.cr_gsa_fqn
}

resource "google_storage_bucket_iam_member" "cr_gsa-output-bucket-reader-iam" {

  bucket       = google_storage_bucket.output-bucket.name
  role         = "roles/storage.legacyBucketReader"
  member       = local.cr_gsa_fqn
}

# Grant access to publishing messages into batches topic
resource "google_pubsub_topic_iam_member" "cr_gsa-batches-iam" {

  project      = var.project_id
  topic        = google_pubsub_topic.topic-batches.id
  role         = "roles/pubsub.publisher"
  member       = local.cr_gsa_fqn
}

# Grant access to reading messages from events subscription
resource "google_pubsub_subscription_iam_member" "cr_gsa-events-iam" {

  project      = var.project_id
  subscription = google_pubsub_subscription.events-subs.id
  role         = "roles/pubsub.subscriber"
  member       = local.cr_gsa_fqn
}

# Grant access to purge events in batches subscription (seek IAM)
resource "google_pubsub_subscription_iam_member" "cr_gsa-batches-subscriber-iam" {

  project      = var.project_id
  subscription = google_pubsub_subscription.batches-subs.id
  role         = "roles/pubsub.subscriber"
  member       = local.cr_gsa_fqn
}
