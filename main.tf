/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Edge Customer's infrastructure creation
#


# Environment check to ensure correct matching of workspace and vars
locals {
  # environment_check = index(terraform.workspace != var.env ? []:[terraform.workspace], terraform.workspace)
}


locals {
  prefix = var.customer_prefix

  # input bucket comes from customer with assets to be processed (predefined)
  # input_bucket  = "${local.prefix}com-vionlabs-bucket-input"
  output_bucket = "${local.prefix}com-vionlabs-bucket-output"
  secure_bucket = "${local.prefix}com-vionlabs-bucket-secure"

  pubsub_topic            = "${local.prefix}com-vionlabs-batches-topic"
  pubsub_topic2           = "${local.prefix}com-vionlabs-events-topic"
  pubsub_subscription     = "${local.prefix}com-vionlabs-batches-subscription"
  pubsub_subscription2    = "${local.prefix}com-vionlabs-events-subscription"

  pubsub_project          = var.project_id

  res_labels = var.tags
}


#
# Input bucket
#
/* should be provided outside this TF

resource "google_storage_bucket" "input-bucket" {
  name     = local.input_bucket
  project  = var.project_id
  location = var.location

  labels = local.res_labels
}
*/

#
# Output bucket
#

resource "google_storage_bucket" "output-bucket" {
  name     = local.output_bucket
  project  = var.project_id
  location = var.location
  uniform_bucket_level_access = true

  labels = local.res_labels
}

#
# Secure bucket
#

resource "google_storage_bucket" "secure-bucket" {
  name     = local.secure_bucket
  project  = var.project_id
  location = var.location
  uniform_bucket_level_access = true

  labels = local.res_labels
}


#
# Create PubSub topics - Forward direction (batches)
#

# 
# All the cloud events will flow through this topic
#
resource "google_pubsub_topic" "topic-batches" {
  name    = local.pubsub_topic
  project = local.pubsub_project

  labels = local.res_labels
}


#
# Create PubSub Subscriptions
# + required IAM bindings after each subscription
#

resource "google_pubsub_subscription" "batches-subs" {
  name  = local.pubsub_subscription
  topic = local.pubsub_topic
  project = local.pubsub_project

  retain_acked_messages = true
  ack_deadline_seconds = 300

  expiration_policy {
    ttl = ""  # never
  }

  labels = local.res_labels

  depends_on = [google_pubsub_topic.topic-batches]
}



#
# Create PubSub topics - Backward direction (events)
#

resource "google_pubsub_topic" "topic-events" {
  name    = local.pubsub_topic2
  project = local.pubsub_project

  labels = local.res_labels
}

#
# Create PubSub Subscriptions
# + required IAM bindings after each subscription
#

resource "google_pubsub_subscription" "events-subs" {
  name  = local.pubsub_subscription2
  topic = local.pubsub_topic2
  project = local.pubsub_project

  retain_acked_messages = true
  ack_deadline_seconds = 300

  expiration_policy {
    ttl = ""  # never
  }

  labels = local.res_labels

  depends_on = [google_pubsub_topic.topic-events]
}
