/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Print some VM related info
#

output "vm_gsa" {
  value = google_service_account.vm_gsa.email
}

output "vm_instance_name" {
  value = google_compute_instance.vionlabs[*].name
}

output "vm_instance_private_ip" {
  value = google_compute_instance.vionlabs[*].network_interface.0.network_ip
}

output "vm_instance_public_ip" {
  value = try(google_compute_instance.vionlabs[*].network_interface.0.access_config.0.nat_ip, "n/a")
}

#
# and some created infrastructure pieces
#

output "location" {
  value = var.location
}

output "gcs_output_bucket" {
  value = google_storage_bucket.output-bucket.name
}

output "gcs_secure_bucket" {
  value = google_storage_bucket.secure-bucket.name
}

output "pubsub_batches_subscription" {
  value = google_pubsub_subscription.batches-subs.name
}

output "pubsub_events_subscription" {
  value = google_pubsub_subscription.events-subs.name
}
