/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Create private VPC networking
#


locals {
  vpc_name      = "vionlabs-vpc"
  subnet_name   = "vionlabs-private-subnet"
  subnet_cidr   = var.vm_net_if_subnet_cidr
  subnet_region = var.location
}


# Create standalone VPC
resource "google_compute_network" "vpc" {

  project                 = local.vm_net_if_subnet_project
  name                    = local.vpc_name
  auto_create_subnetworks = false

  description             = "Vionlabs VPC"
}

# Create private subnet in created VPC
resource "google_compute_subnetwork" "vpc_subnetwork" {

  project                  = google_compute_network.vpc.project
  name                     = local.subnet_name
  ip_cidr_range            = local.subnet_cidr
  region                   = local.subnet_region
  network                  = google_compute_network.vpc.id

  # When enabled, VMs in this subnetwork without external IP addresses
  # can access Google APIs and services by using Private Google Access
  private_ip_google_access = true

  description             = "Vionlabs private subnet"
}
