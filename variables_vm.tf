/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Terraform variables used in main script
#


#
# VM specific variables
#

variable "vm_instance_count" {
    type        = number
    description = "VM instance count to create"
    default     = 1
}

variable "vm_image_name" {
    type = string
    description = "Image name to instantiage VM from"
}

variable "vm_zone" {
    type = string
    description = "VM Instance zone to be running in"
}

variable "vm_net_if_network" {
    type = string
    description = "VM Networking setup: network name"
    default = "default"
}

variable "vm_net_if_subnetwork" {
    type = string
    description = "VM Networking setup: subnetwork name"
    default = "default"
}

variable "vm_net_if_subnet_project" {
    type = string
    description = "VM Networking setup: subnetwork project"
    default = ""
}

variable "vm_net_if_subnet_cidr" {
    type = string
    description = "VM Networking setup: subnetwork CIDR range"
}

variable "vm_ssh_pub_key" {
    type = string
    description = "VM SSH public key"
}

variable "vm_machine_type" {
    type = string
    description = "VM machine configuration type"
    default = "e2-standard-8"  # 8 vCPU 32 GB RAM
}

variable "vm_script" {
    description = "Part of Cloud Init setup script for VM"
    type = string
    default = ""
}
