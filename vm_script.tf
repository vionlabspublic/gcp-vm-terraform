/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


locals {

#
# Construct combined cloud-init + user script
# cloud-init used for early run level usage
# user script is for edge-agent as well as k3s manifests fine-tuning
# this var is to be used by vm instantiation resource
#

  vm_script = <<EOF
Content-Type: multipart/mixed; boundary="===============8752368757152488541=="
MIME-Version: 1.0

--===============8752368757152488541==
Content-Type: text/cloud-config; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-init.yaml"

#cloud-config
cloud_init_modules:
- [scripts-user, always]

--===============8752368757152488541==
Content-Type: text/x-shellscript; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud.sh"

#!/bin/bash
# (c) Vionlabs 2021-2023
#
# Create config file for edge-agent
cat > /opt/vionlabs/edge-agent.env <<EOT
EAGENT_MAX_LOAD=${var.eagent_max_load}
EAGENT_CONN_TYPE=google_pubsub
EAGENT_CONN_CONFIG={"project":"${var.project_id}","topic_name":"${local.pubsub_topic2}","subscription_name":"${local.pubsub_subscription}"}
EAGENT_DELAY_START_SEC=600
EAGENT_BATCHES_WORK_PERIOD_SEC=60
EAGENT_HEARTBEAT_WORK_PERIOD_SEC=600
EAGENT_HEALTH_WORK_PERIOD_SEC=1800
EAGENT_EXIT_AFTER_SEC=43200
EAGENT_APPLOGLEVEL=WARNING
EOT

# Configure k3s maps which depend on external infra
# The destination dir will be auto-executed by k3s on startup
cat > /var/lib/rancher/k3s/server/manifests/app-env.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-env
  namespace: video-ingest
data:
  APPLOGLEVEL: INFO
  ARGOOUTPUTSDIR: /tmp/argo
  ARTIFACTSDIR: "gs://${local.output_bucket}/features"
  EDGE_DEPLOYMENT: "true"
  SECUREARTIFACTSDIR: "gs://${local.secure_bucket}"
EOT

cat > /var/lib/rancher/k3s/server/manifests/cloud-logs-repository.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cloud-logs-repository
  namespace: video-ingest
data:
  cloud-logs-repo: |
    archiveLogs: true
    gcs:
      bucket: ${local.output_bucket}
      keyFormat: logs/{{workflow.name}}/{{pod.name}}
EOT

# Insert customizations (if any) from outside
${var.vm_script}

cat > /var/lib/rancher/k3s/server/manifests/proc-env.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: proc-env
  namespace: video-ingest
data:
  APPLOGLEVEL: INFO
  EDGE_DEPLOYMENT: "true"
  SECUREARTIFACTSDIR: "gs://${local.secure_bucket}"
EOT

--===============8752368757152488541==--
EOF
}
