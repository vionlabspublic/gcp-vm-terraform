/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# VM instantiation
#


locals {
  vm_name = "vm-vionlabs-${lower(random_id.instance_id.id)}"
  vm_tags = ["ssh-vpc"]
  # if subnet proj is empty then use default project
  vm_net_if_subnet_project = var.vm_net_if_subnet_project != "" ? var.vm_net_if_subnet_project : var.project_id
}

#
# Randomize VM naming
#

resource "random_id" "instance_id" {
  byte_length = 4
}

#
# Create requested number of processing VMs
#

resource "google_compute_instance" "vionlabs" {

  count        = var.vm_instance_count

  name         = "${local.vm_name}-${count.index}"
  machine_type = var.vm_machine_type
  project      = var.project_id
  zone         = var.vm_zone

  # Allows Terraform to stop the instance to update its properties.
  # If you try to update a property that requires stopping the instance without setting this field, the update will fail.
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = var.vm_image_name
    }
  }

  network_interface {
    network            = google_compute_network.vpc.name
    subnetwork         = google_compute_subnetwork.vpc_subnetwork.name
    subnetwork_project = local.vm_net_if_subnet_project

    #access_config {
    # Include this section to give the VM an external ip address
    #}
  }

  # Apply the firewall rule to allow external IPs to access this instance by SSH
  tags = local.vm_tags

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.vm_gsa.email
    scopes = ["cloud-platform"]
  }

  metadata = {
    block-project-ssh-keys = true
    ssh-keys        = "ubuntu:${var.vm_ssh_pub_key}"
    user-data       = local.vm_script
  }

  labels = local.res_labels

  depends_on = [local_file.vm_script]
}

#
# Write VM configuration script into file
#

resource "local_file" "vm_script" {
  content  = local.vm_script
  filename = "vm-startup-script.cfg"
}
