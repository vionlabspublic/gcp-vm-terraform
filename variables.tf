/*
* Unpublished work.
* Copyright 2021-2022 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Terraform variables used in main script
#


variable "project_id" {
    type = string
    description = "Destination Google Project ID"
}

variable "location" {
    type = string
    description = "Location (region) of output bucket"
}

variable "backend_service_account" {
    type = string
    description = "GSA to be used by Vionlabs Backend instance. Shall have all the required rights for object it talks to"
}

variable "env" {
    type = string
    description = "Environment - Used for assertion checks"
    default = "error"
}

variable "tags" {
    type = map
    description = "a set of tags to be used on compatible resources"
    default = {}
}

variable "customer_prefix" {
    type = string
    description = "Prefix to prepend before customer's resource names"
}

variable "eagent_max_load" {
  description = "Infrastructure: Edge Agent number of parallel processings"
  type        = string
  default     = "2"
}
